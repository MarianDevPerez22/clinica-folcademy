package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service("medicoService")
public class MedicoService {
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;

    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
    }

    public List<MedicoDto> findAll() {
        return medicoRepository.findAll().stream().map(medicoMapper::entityToDto).collect(Collectors.toList());
    }
    public MedicoDto findById(Integer id) {
        return medicoRepository.findById(id).map(medicoMapper::entityToDto).orElseThrow(() -> new NotFoundException("Medico inexistente"));
    }
    public MedicoDto create(MedicoDto dto) {
        dto.setId(null);
        if(dto.getConsulta()<0)
            throw new BadRequestException("La consulta no puede ser menor a 0");
        return medicoMapper.entityToDto(medicoRepository.save(medicoMapper.dtoToEntity(dto)));
    }
    public MedicoDto edit(Integer id, MedicoDto dto) {
        if (!medicoRepository.existsById(id))
            throw new NotFoundException("Medico inexistente");
        if(dto.getConsulta()<0)
            throw new BadRequestException("La consulta no puede ser menor a 0");
        dto.setId(id);
        return medicoMapper.entityToDto(medicoRepository.save(medicoMapper.dtoToEntity(dto)));
    }
    public Boolean delete(Integer id) {
        if (!medicoRepository.existsById(id))
            throw new NotFoundException("Medico inexistente");;
        medicoRepository.deleteById(id);
        return true;
    }
}
