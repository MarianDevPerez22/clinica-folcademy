package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("turnoService")
public class TurnoService {
    private final TurnoRepository turnoRepository;
    private final TurnoMapper turnoMapper;
    private final PacienteRepository pacienteRepository;
    private final MedicoRepository medicoRepository;

    public TurnoService(TurnoRepository turnoRepository, TurnoMapper turnoMapper, PacienteRepository pacienteRepository, MedicoRepository medicoRepository) {
        this.turnoRepository = turnoRepository;
        this.turnoMapper = turnoMapper;
        this.pacienteRepository = pacienteRepository;
        this.medicoRepository = medicoRepository;
    }


    public List<TurnoDto> findAll() {
        return turnoRepository.findAll().stream().map(turnoMapper::entityToDto).collect(Collectors.toList());
    }
    public TurnoDto findById(Integer id) {
        return turnoRepository.findById(id).map(turnoMapper::entityToDto).orElseThrow(() -> new NotFoundException("Turno inexistente"));
    }
    public TurnoDto create(TurnoDto dto) {
        dto.setIdturno(null);
        if (!pacienteRepository.existsById(dto.getIdpaciente()))
            throw new NotFoundException("Paciente inexistente");
        if (!medicoRepository.existsById(dto.getIdmedico()))
            throw new NotFoundException("Medico inexistente");
        return turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(dto)));
    }
    public TurnoDto edit(Integer id, TurnoDto dto) {
        if (!turnoRepository.existsById(id))
            throw new NotFoundException("Turno inexistente");
        if (!pacienteRepository.existsById(dto.getIdpaciente()))
            throw new NotFoundException("Paciente inexistente");
        if (!medicoRepository.existsById(dto.getIdmedico()))
            throw new NotFoundException("Medico inexistente");
        dto.setIdturno(id);
        return turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(dto)));
    }
    public Boolean delete(Integer id) {
        if (!turnoRepository.existsById(id))
            throw new NotFoundException("Turno inexistente");;
        turnoRepository.deleteById(id);
        return true;
    }
}
