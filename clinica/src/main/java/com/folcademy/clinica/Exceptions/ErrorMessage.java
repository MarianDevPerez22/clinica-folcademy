package com.folcademy.clinica.Exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ErrorMessage {
    private String message;
    private String detail;
    private String code;
    private String path;
}
