package com.folcademy.clinica.Exceptions;

public class FailedVerificationException extends Exception {
    public FailedVerificationException(String message) {
        super(message);
    }
}
