package com.folcademy.clinica.Model.Entities;

import lombok.*;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
@Entity
@Table(name = "turno")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Turno {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idturno", columnDefinition = "INT(10) UNSIGNED")
    public Integer idturno;
	@Column(name = "fecha", columnDefinition = "DATE")
    public LocalDate fecha;
	@Column(name = "hora", columnDefinition = "TIME")
    public LocalDateTime hora;
	@Column(name = "atendido", columnDefinition = "TINYINT")
    public Boolean atendido = false;
	@Column(name = "idpaciente", columnDefinition = "INT(10) UNSIGNED")
    public Integer idpaciente;
	@Column(name = "idmedico", columnDefinition = "INT(10) UNSIGNED")
    public Integer idmedico;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idpaciente", referencedColumnName = "idpaciente", insertable = false, updatable = false)
    private Paciente paciente;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idmedico", referencedColumnName = "idmedico", insertable = false, updatable = false)
    private Medico medico;
}
