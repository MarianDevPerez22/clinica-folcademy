package com.folcademy.clinica.Model.Dtos;

import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TurnoDto {
    @NotNull
    public Integer idturno;
    public LocalDate fecha;
    public LocalDateTime hora;
    public Boolean atendido;
    @NotNull
    public Integer idpaciente;
    @NotNull
    public Integer idmedico;
    private Paciente paciente;
    private Medico medico;
}
