package com.folcademy.clinica.Model.Entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "medico")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Medico {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idmedico", columnDefinition = "INT(10) UNSIGNED")
    public Integer id;
    @Column(name = "nombre", columnDefinition = "VARCHAR")
    public String nombre = "";
    @Column(name = "apellido", columnDefinition = "VARCHAR")
    public String apellido = "";
    @Column(name = "profesion", columnDefinition = "VARCHAR")
    public String profesion = "";
    @Column(name = "consulta", columnDefinition = "INT(10) UNSIGNED")
    public Integer consulta = 0;
}
