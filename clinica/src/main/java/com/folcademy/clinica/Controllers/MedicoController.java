package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.FailedVerificationException;
import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medicos")
public class MedicoController {
    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }

    @GetMapping("")
    public ResponseEntity<List<MedicoDto>> findAll(){
        return ResponseEntity.ok(medicoService.findAll());
    }
    @GetMapping("/{idMedico}")
    public ResponseEntity<MedicoDto> findById(@PathVariable(name = "idMedico") int id){
        return ResponseEntity.ok(medicoService.findById(id));
    }
    @PostMapping("")
    public ResponseEntity<MedicoDto> create(@RequestBody @Validated MedicoDto dto){
        return ResponseEntity.ok(medicoService.create(dto));
    }
    @PutMapping("/{idMedico}")
    public ResponseEntity<MedicoDto> edit (@PathVariable(name = "idMedico") int id,
                                           @RequestBody @Validated MedicoDto dto) {
        return ResponseEntity.ok(medicoService.edit(id, dto));
    }
    @DeleteMapping("/{idMedico}")
    public ResponseEntity<Boolean> delete (@PathVariable(name = "idMedico") int id) {
        return ResponseEntity.ok(medicoService.delete(id));
    }

}
