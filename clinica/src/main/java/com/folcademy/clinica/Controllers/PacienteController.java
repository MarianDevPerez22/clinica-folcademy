package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pacientes")
public class PacienteController {

    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }


    @GetMapping(value = "")
    public ResponseEntity<List<PacienteDto>> findAll() {
        return ResponseEntity
                .ok(pacienteService.findAll());
    }

    @GetMapping(value = "/{idPaciente}")
    public ResponseEntity<PacienteDto> findAll(@PathVariable(name = "idPaciente") Integer id) {
        return ResponseEntity
                .ok(pacienteService.findById(id));
    }
    @PostMapping("")
    public ResponseEntity<PacienteDto> create(@RequestBody @Validated PacienteDto dto) {
        return ResponseEntity.ok(pacienteService.create(dto));
    }
    @PutMapping("/{idMedico}")
    public ResponseEntity<PacienteDto> edit (@PathVariable(name = "idMedico") int id,
                                           @RequestBody @Validated PacienteDto dto) {
        return ResponseEntity.ok(pacienteService.edit(id, dto));
    }
    @DeleteMapping("/{idMedico}")
    public ResponseEntity<Boolean> delete (@PathVariable(name = "idMedico") int id) {
        return ResponseEntity.ok(pacienteService.delete(id));
    }
}
