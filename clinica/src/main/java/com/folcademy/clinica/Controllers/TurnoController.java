package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/turnos")
public class TurnoController {
    private final TurnoService turnoService;

    public TurnoController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }
    @GetMapping("")
    public ResponseEntity<List<TurnoDto>> findAll(){
        return ResponseEntity.ok(turnoService.findAll());
    }
    @GetMapping("/{idTurno}")
    public ResponseEntity<TurnoDto> findById(@PathVariable(name = "idTurno") int id){
        return ResponseEntity.ok(turnoService.findById(id));
    }
    @PostMapping("")
    public ResponseEntity<TurnoDto> create(@RequestBody @Validated TurnoDto dto){
        return ResponseEntity.ok(turnoService.create(dto));
    }
    @PutMapping("/{idTurno}")
    public ResponseEntity<TurnoDto> edit (@PathVariable(name = "idTurno") int id,
                                           @RequestBody @Validated TurnoDto dto) {
        return ResponseEntity.ok(turnoService.edit(id, dto));
    }
    @DeleteMapping("/{idTurno}")
    public ResponseEntity<Boolean> delete (@PathVariable(name = "idTurno") int id) {
        return ResponseEntity.ok(turnoService.delete(id));
    }
}
